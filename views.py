
from django.shortcuts import render_to_response
from django.http import HttpResponseBadRequest , HttpResponse
import pyexcel.ext.xlsx
from models import Question , Choice

from django.template import RequestContext
from django import forms

class UploadFileForm(forms.Form):
 file = forms.FileField(label='')

def import_data(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            file_name = request.FILES['file']
            sheet = file_name.get_sheet()
            records = sheet.to_records()
            for record in records:
                keys = record.keys()
                key_1 = keys[1]
                quest_text = record[key_1]
                key_2 = keys[2]
                choice1 = record[key_2]; key_3 = keys[3]
                choice2 = record[key_3]
                key_4 = keys[4]
                choice3 = record[key_4]
                key_0 = keys[0]
                choice4 = record[key_0]
                key_5 = keys[5]
                answer= record[key_5]
                q=Question(quest_text,answer)
                q.save()
                id_quest = q.id
                c1 = Choice(id_quest,choice1)
                c1.save()
                c2 = Choice(id_quest,choice2)
                c2.save()
                c3 = Choice(id_quest,choice3)
                c3.save()
                c4 = Choice(id_quest,choice4)
                c4.save()
            return HttpResponse("OK", status=200)
        else:
            return HttpResponseBadRequest()


    else:
        form = UploadFileForm()

    return render_to_response(
        'upload_form.html',
        {
                'form': form,
                'title': 'Import excel data into database example',
                'header': 'Please upload sample-data.xls:'
        },
            context_instance=RequestContext(request))