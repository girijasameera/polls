DEPENDENCIES

1.Django 1.9
2.Pycharm 5.0.2
3.Python 2.7
4.Git 

INSTALLATION

1.Go to https://gitlab.com/girijasameera/polls .
2.Click on HTTPS to get the address to use for cloning.
3.Open Pycharm and go to VCS in the menu bar 
    a)Click on Checkout from Version Control.
    b)Click on Git
    c)Paste the URL provided in Step 2 and Press OK.
4.Go to Tools and Choose run manage.py task
5.Enter the following command:runserver and press enter.
6.Go to http://127.0.0.1:8000/admin/ to access admin page and login using-
        username:girija
        password:Girija123
7.Go to http://127.0.0.1:8000/polls/ to see the actual application
