from django.conf.urls import url

from . import views

app_name = 'polls'
urlpatterns = [
   url(r'^import/', views.import_data, name="import"),
]